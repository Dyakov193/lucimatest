﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LacimaTest.Helpers;

namespace UnitTest_LucimaTest
{
    [TestClass]
    public class CsvReaderTest
    {

        private const int Expected = 16;
        [TestMethod]
        public void ReadingTest()
        {
            var collection = CsvReader.ReadCsvFileInTabular("in.csv");
            Assert.AreEqual(Expected, collection.Count);
        }
    }
}
