﻿//static class for reading data rows
//and coverting csv data row to GasPriceRow model item
//and holding it in List collection

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;

using LacimaTest.Models;
using System.Globalization;

namespace LacimaTest.Helpers
{
    public static class CsvReader
    {
        public static List<GasPriceRow> ReadCsvFileInTabular(string filePath) // methon for reading csv in tabular format
        {
            List<GasPriceRow> GasPriceRowCollection = new List<GasPriceRow>();

            int rowIndex = 0;
            StreamReader streamReader = new StreamReader(filePath);

            string []splittedCsvRow;
            while (!streamReader.EndOfStream) // reading line by line till the end of the file
            {
                // skipping header row
                if (rowIndex == 0) 
                {
                    streamReader.ReadLine();
                    rowIndex++; 
                    continue; 
                }

                try
                {
                    splittedCsvRow = streamReader.ReadLine().Split(','); // reading row and splitting it by separator

                    //converting row to model item and putting it to collection
                    GasPriceRowCollection.Add(new GasPriceRow()
                    {
                        ObservationDate = DateTime.ParseExact(splittedCsvRow[0], "dd/MM/yyyy", CultureInfo.InvariantCulture),
                        Shorthand = splittedCsvRow[1],
                        From = DateTime.ParseExact(splittedCsvRow[2], "dd/MM/yyyy", CultureInfo.InvariantCulture),
                        To = DateTime.ParseExact(splittedCsvRow[3], "dd/MM/yyyy", CultureInfo.InvariantCulture),
                        Price = Double.Parse(splittedCsvRow[4].Replace(',', '.'), CultureInfo.InvariantCulture)
                    });
                }
                catch
                {
                    continue;
                }

            }

            // free memory
            streamReader.Close();
            streamReader.Dispose();

            return GasPriceRowCollection; 
        }
    }
}
