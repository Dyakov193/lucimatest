﻿//static class for grouping and writing data

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

using LacimaTest.Models;

namespace LacimaTest.Helpers
{
    public static class CsvWriter
    {
        static List<string> ShorthandsCollection = new List<string>();
        public static void WriteCsvFileInGrid(List<GasPriceRow> GasPriceRowCollection, string filePath) // method to execute grouping and saving data to csv
        {
            
            StreamWriter streamWriter = new StreamWriter(filePath, false, Encoding.Default); 

            //writing header for file
            streamWriter.Write("ObrvationDate,");
            foreach(var columnGroup in GasPriceRowCollection.GroupBy(x => x.Shorthand))
            {
                streamWriter.Write(columnGroup.Key + ",");
                ShorthandsCollection.Add(columnGroup.Key);
            }
            streamWriter.WriteLine();

            //grouping by date and saving data
            foreach (var RowsForSpecDate in GasPriceRowCollection.GroupBy(x => x.ObservationDate))
            {
                try
                {
                    streamWriter.Write(RowsForSpecDate.FirstOrDefault().ObservationDate.ToString("dd/MM/yyyy"));

                    foreach (var shorthand in ShorthandsCollection)
                    {
                        string price = "";
                        if(RowsForSpecDate.Where(x => x.Shorthand == shorthand).Count() > 0)
                            price = RowsForSpecDate.Where(x => x.Shorthand == shorthand).FirstOrDefault().Price.ToString().Replace(',', '.');
                                              
                        streamWriter.Write("," + price);

                    }
                    streamWriter.WriteLine();
                }
                catch
                {
                    continue;
                }
            }

            //free memory
            streamWriter.Close();
            streamWriter.Dispose();
        }



    }
}
