﻿///Model for reading and holding data from row in csv file

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacimaTest.Models
{
    public class GasPriceRow
    {
        public DateTime ObservationDate { get; set; }
        public string Shorthand { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public double Price { get; set; }
    }
}
