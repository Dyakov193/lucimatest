﻿using LacimaTest.Helpers;
using LacimaTest.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacimaTest
{
    class Program
    {
        static void Main(string[] args)
        {

            Stopwatch stopwatch = Stopwatch.StartNew();

            Console.WriteLine(DateTime.Now.ToShortTimeString() + ": Reading csv file {0}" + args[0]);
            List<GasPriceRow> gasPriceRows = CsvReader.ReadCsvFileInTabular(args[0]);

            Console.WriteLine(DateTime.Now.ToShortTimeString() + ": Csv file loaded. It contains {0} rows of data", gasPriceRows.Count);
            CsvWriter.WriteCsvFileInGrid(gasPriceRows, args[0]);

            Console.WriteLine(DateTime.Now.ToShortTimeString() + ": Csv file {0} saved.", args[1]);

            stopwatch.Stop();
            Console.WriteLine(DateTime.Now.ToShortTimeString() + "It takes {0} milliseconds to complete the job", stopwatch.ElapsedMilliseconds);
        }
    }
}
